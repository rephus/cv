## Description

This repository is used to host my personal CV in all formats, along with the code to generate the CV in Json format using NodeJs.

The CV is written in Markdown format, in that way I can export it to any other format using __pandoc__

The Javascript code parses the markdown file to generate the Json output, feel free to look at the code if you want.

Run *npm install* to install nodejs dependencies (look at package.json for more info)

## For more information

* [See the introduction page](http://javi-cv.herokuapp.com)

* [See the CV in Json](http://javi-cv.herokuapp.com/json)

* [See the CV in markdown format](https://bitbucket.org/rephus/cv/src/5846e828eae04cbfa069e5144be55839429169d7/cv.md?at=master)
* [See the CV in Html](http://javi-cv.herokuapp.com/cv.html)
* [Download the CV in ODT format](http://javi-cv.herokuapp.com/cv.odt)
* [Download the CV in PDF format](http://javi-cv.herokuapp.com/cv.pdf)