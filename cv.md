<!--
Everything in comments works in node.js and output to json,
but markdown will ignore those lines.

Also, everything between parenthesis and without semicolons 
will be ignored by node.js / json
-->

Javier Rengel Jiménez

Backend software engineer and indie games developer

## Personal information

<!--
Name: Javier Rengel Jimenez
Title: Backend software engineer and indie games developer
-->

### Languages

* spanish (Native)
* english (High) 

### Contact

Email: rephus@gmail.com / javier@coconauts.net

Telephone:  +44 7435230551

LinkedIn: [Javier Rengel](https://www.linkedin.com/pub/javier-rengel-jimenez/56/865/432/en)

Twitter: [rephus](http://twitter.com/rephus)

Facebook: [rephus](https://www.facebook.com/rephus)

## Education

* BSc in Computer Engineering (Ingeniería Técnica en Informática de Gestión) at Málaga University, with honors (2006-2012)
* Course in graphics processors of the family ATI Radeon and Nvidia Gforce at Málaga University (2008, 40 hours)
* Course in PC assembly and configuring  at Málaga University (2008, 40 hours).

## Skills

### Languages

* Java / J2EE (High)
* Scala (High)
* Javascript / JQuery (High)
* Html, Css3 (High)
* Bash scripting (High)
* PHP (Medium)
* Android (Medium)
* C++ (Basic)
* Python (Basic)
* TcL / Expect (Basic) 

### Frameworks

* Apache (High)
* Sbt / Play (High)
* JUnit (High)
* LibGDX (High)
* Jenkins (Medium)
* NodeJS (Medium)
* Nginx (Medium)

### Databases

* Mysql
* Oracle
* SqLite
* Cassandra
* Redis
* ElasticSearch

## Attitudes

* Enthusiast and curious: continously learning and trying new technologies and languages
* High-speed learner
* Motivation to solve difficult problems
* Friendly person, excellent working with teams, as well as independently
* Result oriented
* Strong work ethics
* Always looking for efficiency
* Ability to explain concepts in a clear way, both at technical and non-technical level

## Working experience

### Top10 ( 2013-05-01 / - )

Url: http://top10.com
<!--
Started: 2013-05-01 
-->
Description: Top10 is a hotel metasearch startup company based in London, taking advantage of brand new web technologies and good marketing campaigns.

Conclussion: This small startup hires really good engineers, so I improved my knowledge in new technologies and good code practices 

#### Tasks

* Web backend development in Scala (sbt)
* Full custom CMS app built in Scala (play) 
* CI administration with Jenkins
* DB administration (Cassandra, Memcached, ElasticSearch, Redis)
* Heavy use of Amazon WS

### Openbet Ltd.  ( 2012-10-15 / 2013-05-01 )

Url: http://www.openbet.com

<!--
Started: 2012-10-15

Finished: 2013-05-01
-->

Description: Openbet are the world's leading provider of interactive gaming and betting solutions. Established in 1996, and with offices in USA, Australia and the UK, provides solutions for all of the mayor companies in the online gaming industry, including William Hill, Ladbrokes, Paddy Power, or Betfair.

Conclussion: The codebase and the project was not so exciting, but I learned how it feels working in a large company, with a large team and I also was introduced to agile (standups, code reviews) 

#### Tasks

* Web backend development for sportsbook sites in Tcl
* Troubleshooting, QA, optimization and code analysis for existing products
* Automation scripts in Bash and Expect
* Tests server management (using Puppet and Bash scripting)
* Release manager: in charge or the team's code merging, code deployment, and direct support towards clients

### Asesores Locales consultoria S.A. ( 2009-07-01 / 2012-09-25 )

Url: http://www.asesoreslocales.com

<!--
Started: 2009-07-01 

Finished: 2012-09-25
-->

Description: Asesores locales is a well renowned medium-size consultancy company based in Madrid, Malaga and Cadiz (Spain). It's main line of business is the development of software for local goverments, as well as training in several fields. Other clients include companies both spanish and international.

Conclussion: Asesores Locales was my first job as a developer. I learn some basics and JEE, and thanks to my motivation I ended up learning new stuff and theaching others, in other words, I was the innovative and geed mind in the company. I also built (by myself or leading) some applications and servers required.

#### Tasks

* JEE development in several projects, as part of teams and autonomously
* UI with IceFaces, Jboss Richfaces, ADF and Woodstock frameworks
* GIS system development in JEE using MapServer for custom street location service
* Installation, configuration and maintenance of IBM servers with Windows Server 2008 R2 and CentOS
* Administration of Oracle, MySQL and PostgreSQL databases
* Application servers Glassfish v2/v3 and Oracle weblogic
* VMWare VSphere hypervisor 5.0 administration
* Product presentations (inside and outside the company)
* Internal training
* Manager role in some projects

## Personal projects

* Owner of the Internet domain http://coconauts.net and contributor of http://cineactual.net
* Development of various videogames and applications for Android since 1.6
* Creator of the commercial page [Cleope](http://cleopealhaurin.es)
* Creator of the collaborative forum for regional inspectors [Interventores](http://interventores.info)
* Musical videogame developed in Java and SDL as final thesis
* Casual mobile videogames released for android platform [android](https://play.google.com/store/apps/developer?id=Javier%20Rengel&hl=es_419)
* Videogame experiments with Unity 3D and HTML5
* Participation in software contests and jams like Ludum dare, CUSL or Biicode
* Personal electronic projects using Arduino and Raspberry PI
* Personal scripts and webpages
* Design and construction of domestic furniture with AutoCAD
